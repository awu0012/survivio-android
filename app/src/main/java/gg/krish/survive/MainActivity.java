package gg.krish.survive;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private GameView gameView;

    class MyButton {
        Button b;
        ArrayList<Integer> keyEvents;
        MyButton(Button _b, ArrayList<Integer> _keyEvents) {
            b = _b;
            keyEvents = _keyEvents;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        gameView = findViewById(R.id.webview);
        gameView.setWebViewClient(new WebViewClient());
        gameView.getSettings().setJavaScriptEnabled(true);
        gameView.loadUrl("http://surviv.io/");
//        gameView.evaluateJavascript("document.getElementById('game-area-wrapper').bind('touchstart', function() {});", new ValueCallback<String>() {
//            @Override
//            public void onReceiveValue(String value) {
//                System.out.println("km> value " + value);
//            }
//        });

        ArrayList<MyButton> actionButtons = new ArrayList();


        final MyButton fBtn = new MyButton((Button)findViewById(R.id.f_btn),
                new ArrayList<Integer>(Arrays.asList(KeyEvent.KEYCODE_F)));
        final MyButton eBtn = new MyButton((Button)findViewById(R.id.e_btn),
                new ArrayList<Integer>(Arrays.asList(KeyEvent.KEYCODE_E)));

        actionButtons.add(fBtn);
        actionButtons.add(eBtn);


        actionButtons.add(new MyButton((Button)findViewById(R.id.s_btn),
                new ArrayList<Integer>(Arrays.asList(KeyEvent.KEYCODE_S))));
        actionButtons.add(new MyButton((Button)findViewById(R.id.a_btn),
                new ArrayList<Integer>(Arrays.asList(KeyEvent.KEYCODE_A))));
        actionButtons.add(new MyButton((Button)findViewById(R.id.d_btn),
                new ArrayList<Integer>(Arrays.asList(KeyEvent.KEYCODE_D))));
        actionButtons.add(new MyButton((Button)findViewById(R.id.w_btn),
                new ArrayList<Integer>(Arrays.asList(KeyEvent.KEYCODE_W))));
        actionButtons.add(new MyButton((Button)findViewById(R.id.ds_btn),
                new ArrayList<Integer>(Arrays.asList(KeyEvent.KEYCODE_D, KeyEvent.KEYCODE_S))));
        actionButtons.add(new MyButton((Button)findViewById(R.id.as_btn),
                new ArrayList<Integer>(Arrays.asList(KeyEvent.KEYCODE_A, KeyEvent.KEYCODE_S))));
        actionButtons.add(new MyButton((Button)findViewById(R.id.aw_btn),
                new ArrayList<Integer>(Arrays.asList(KeyEvent.KEYCODE_A, KeyEvent.KEYCODE_W))));
        actionButtons.add(new MyButton((Button)findViewById(R.id.dw_btn),
                new ArrayList<Integer>(Arrays.asList(KeyEvent.KEYCODE_D, KeyEvent.KEYCODE_W))));


        for (final MyButton mButton : actionButtons) {
            mButton.b.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        for (Integer key : mButton.keyEvents) {
                            gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, key));
                        }
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        for (Integer key : mButton.keyEvents) {
                            gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, key));
                        }
                    }
                    return true;
                }
            });
        }


        Button escapeBtn = findViewById(R.id.escape_btn);
        escapeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ESCAPE));
                gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_ESCAPE));
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            gameView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }
}
